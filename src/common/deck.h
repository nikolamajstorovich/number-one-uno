#ifndef DECK_H
#define DECK_H

#include <vector>
#include <algorithm>
#include <random>

#include "card.h"

class Deck
{
public:
    Deck();
    void shuffle();
    Card giveCard();
    Card topCard();
    void addCard(Card);
    bool isEmpty();

private:
    QVector<Card> m_Cards;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

};
#endif // DECK_H
