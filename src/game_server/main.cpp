#include "mainserver.h"

#include <QCoreApplication>

auto main(int argc, char *argv[]) -> int
{
    QCoreApplication a(argc, argv);

    MainServer server;
    if (server.listen(QHostAddress::Any, 1967)) {
        qDebug() << "Server listening on port" << server.serverPort();
    } else {
        qCritical() << "Failed to start server:" << server.errorString();
        return 1;
    }

    return a.exec();
}
