cmake_minimum_required(VERSION 3.14)

project(game_server LANGUAGES CXX)

include_directories(${CMAKE_SOURCE_DIR}/../src)

message("CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}")

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Network)

set(SOURCES
    main.cpp
    mainserver.cpp
    gameworker.cpp
)

set(COMMON_SOURCES
    ../common/game.cpp
    ../common/dealerservice.cpp
    ../common/player.cpp
    ../common/card.cpp
    ../common/pile.cpp
    ../common/deck.cpp
)

add_executable(game_server
    ${SOURCES}
    ${COMMON_SOURCES}
)

target_link_libraries(game_server
    PRIVATE
        Qt${QT_VERSION_MAJOR}::Core
        Qt${QT_VERSION_MAJOR}::Network
)

target_include_directories(game_server PRIVATE ../common)

target_include_directories(game_server PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

include(GNUInstallDirs)
install(TARGETS game_server
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
