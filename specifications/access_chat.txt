𝐎𝐩𝐢𝐬 𝐬𝐥𝐮𝐜𝐚𝐣𝐚 𝐮𝐩𝐨𝐭𝐫𝐞𝐛𝐞 𝐂𝐡𝐚𝐭
 
𝐊𝐫𝐚𝐭𝐚𝐤 𝐨𝐩𝐢𝐬: Omogucava komunikaciju izmedju igraca dok su u igri.

𝐀𝐤𝐭𝐞𝐫𝐢:
Igrac - igra 1 partiju igre

𝐏𝐫𝐞𝐝𝐮𝐬𝐥𝐨𝐯𝐢: Aplikacija je pokrenuta i startovana je 1 partija

𝐏𝐨𝐬𝐭𝐮𝐬𝐥𝐨𝐯𝐢: /

𝐎𝐬𝐧𝐨𝐯𝐧𝐢 𝐭𝐨𝐤:

1. Igrac klikne na polje za unos teksta 
2. Igrac unosi tekst(poruku) u polje za unos teksta
3. Igrac klikne na dugme ENTER kako bi poslao poruku
4. klijent prosledjuje serveru informacije o username-u igraca koji je poslao poruku, sam tekst poruke, vreme kada je poruka poslata
5. server proverava da li su poslate informacije validne
	5.1. Ako tekst poruke nije validan ili username igraca koji je poslao poruku nije validan ili vreme kada je poruka poslata nije validno
		5.1.1. server salje klijentu informaciju da poruka nije validna, a igracima se ne prikazuje poruka  
	5.2. Inace server formatira poruku i prosledjuje je klijentu
6. Aplikacija prikazuje  ostalim igracima tekst(poruku), username igraca koji je poslao poruku i vreme kada je poruka poslata tj. prikazuje formatiranu poruku


𝐀𝐥𝐭𝐞𝐫𝐧𝐚𝐭𝐢𝐯𝐧𝐢 𝐭𝐨𝐤𝐨𝐯𝐢: /

𝐏𝐨𝐝𝐭𝐨𝐤𝐨𝐯𝐢: /

𝐒𝐩𝐞𝐜𝐢𝐣𝐚𝐥𝐧𝐢 𝐳𝐚𝐡𝐭𝐞𝐯𝐢: Potrebna je veza sa serverom

𝐃𝐨𝐝𝐚𝐭𝐧𝐞 𝐢𝐧𝐟𝐨𝐫𝐦𝐚𝐜𝐢𝐣𝐞: Poruke koje su poslate nece biti sacuvane vec samo prikazane
