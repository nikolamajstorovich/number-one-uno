#ifndef SERVERCOMMUNICATIONMANAGER_H
#define SERVERCOMMUNICATIONMANAGER_H

#include <QObject>
#include <QTcpSocket>

class ServerCommunicationManager : public QObject
{
    Q_OBJECT

public:
    static ServerCommunicationManager& instance()
    {
        static ServerCommunicationManager instance;
        return instance;
    }

    bool connectToServer(const QString &serverAddress, int port);
    bool isConnected();

    void sendRequest(const QJsonObject &json);

    void createGame(QString username);
    void joinGame(QString username, QString gameID);
    void startGame();

public slots:
    void onDeckClicked(int playerIndex);
    void onCardClicked(int playerIndex, int handCardIndex);
    void onDialogWildCardClicked(QString color);
    void onUnoClicked(int playerIndex);
    void onMsgClicked(int playerIndex,QString txt);


private slots:
    void handleError(QAbstractSocket::SocketError socketError);
    void receiveJson();

signals:
    void goToLobby(QString gameID, QVector<QString> players);
    void joinLobby(QString gameID, QVector<QString> players);
    void startGameSignal(QVector<QString> players);
    void cardDealtSignal(int playerIndex, QString cardType, QString color, int value);
    void cardPlayedSignal(int playerIndex, int cardIndex);
    void initializePileSignal(QString cardType, QString color, int value);
    void showError(QString msg);
    void setCurrentPlayer(int currPlayerIdx);
    void setColorSignal(QString color);
    void unoCalledSignal(int playerIndex);
    void gameOver(int winner);
    void sendMsgSignal(int playerIndex,QString msg);
    void goToStartScreenSignal();


private:
    void parseJson(const QJsonObject &json);

private:
    ServerCommunicationManager();
    QTcpSocket* socket;
};

#endif // SERVERCOMMUNICATIONMANAGER_H
