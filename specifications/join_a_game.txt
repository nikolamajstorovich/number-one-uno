Slučaj upotrebe: Pridruži se igri (Join a game)

Kratak opis: Igrač se pridružuje već postojecoj partiji.

Akteri: 
- Igrač
- Server

Preduslovi: 
Aplikacija je pokrenuta i igrač je pritisnuo dugme "Pridruži se igri".

Postuslovi: Podešavanja su sačuvana i igrač je pridružen igri.

Osnovi tok:
1. Aplikacija prikazuje prozor sa podešavanjima.
2. Igrač unosi svoje ime u polje "Username".
3. Igrač unosi ID igre u polje "ID igre".
4. Igrač pritiska dugme "Pridruži se partiji"
	4.1. Aplikacija proverava sa serverom da li postoji partija sa datim ID-jem
		4.1.1. Ukoliko ne postoji partija sa datim ID-jem:
			4.1.1.1. Aplikacija prikazuje poruku o nepostojećoj partiji 
			4.1.1.2. Prelazi se na korak 3.
5. Aplikacija dohvata unešene podatke i pridružuje igraca u partiju sa datim ID-jem. 

Podtokovi: /

Alternativni tokovi: /

Specijalni zahtevi: Aplikacija je povezana na server radi umrežavanja igrača.

Dodatne informacije: /
