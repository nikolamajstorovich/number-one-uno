#include "pile.h"

Pile::Pile()
    : m_Cards(QVector<Card>())
{}

auto Pile::topCard() -> Card
{
    return m_Cards.last();
}

auto Pile::giveCard() -> Card
{
    Card card = m_Cards.last();
    m_Cards.pop_back();
    return card;
}

void Pile::addCard(Card& card)
{
    m_Cards.push_back(card);
}

auto Pile::isEmpty() -> bool
{
    return m_Cards.empty();
}

auto Pile::getTopCardColor() const -> QString
{
    return topCardColor;
}

void Pile::setTopCardColor(const QString &newTopCardColor)
{
    topCardColor = newTopCardColor;
}
