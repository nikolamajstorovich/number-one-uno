#include "playeritem.h"
#include "carditem.h"
#include <QGraphicsSceneMouseEvent>

#include <iostream>

PlayerItem::PlayerItem(QObject *parent)
    : QGraphicsScene(parent)
{}

void PlayerItem::newCardAdded(CardItem *item, int index)
{
    if (m_playerIndex == index)
    {
        m_cardItems.append(item);
        NewItemPosition(item);
        updateSceneRect();
    }
}


void PlayerItem::NewItemPosition(CardItem *item)
{
    auto newItemIndex = 0;

    if (m_cardItems.size() != 0)
    {
        newItemIndex = m_cardItems.size() - 1;
    }

    const auto xPos = item->Width() * newItemIndex * m_cardCoef;
    const auto yPos = 0;

    item->setZValue(newItemIndex);
    item->setPos(xPos, yPos);
}

QVector<CardItem *> PlayerItem::cardItems() const
{
    return m_cardItems;
}

void PlayerItem::RemoveCard(CardItem *item)
{
    if(item && this->items().contains(item))
    {
        this->removeItem(item);
        m_cardItems.removeOne(item);
        delete item;

        updateSceneRect();
        updateCardPositions();
    }
}

void PlayerItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->isAccepted())
    {
        return;
    }

    QGraphicsItem* clickedGraphicsItem = itemAt(event->scenePos(), QTransform());

    CardItem* clickedItem = dynamic_cast<CardItem *>(clickedGraphicsItem);

    if (clickedItem && clickedItem->visible()== true)
    {
        event->accept();
        emit CardPressed(clickedItem);
    }
}

void PlayerItem::updateCardPositions()
{
    auto xPos = 0;
    const auto yPos = 0;

    int i = 0;
    for (auto cardItem : m_cardItems)
    {
        CardItem* card = dynamic_cast<CardItem *>(cardItem);
        i = m_cardItems.indexOf(cardItem);

        card->setZValue(i);

        xPos = card->Width() * i * m_cardCoef;
        card->setPos(xPos, yPos);
    }
}

int PlayerItem::playerIndex()
{
    return m_playerIndex;
}

void PlayerItem::setPlayerIndex(int playerIndex)
{
    m_playerIndex = playerIndex;
}

void PlayerItem::updateSceneRect()
{
    if (!m_cardItems.isEmpty())
    {
        // Get the dimensions of a CardItem
        int cardWidth = m_cardItems.first()->boundingRect().width();
        int cardHeight = m_cardItems.first()->boundingRect().height();

        // Calculate the total width and height based on the number of cards
        int totalWidth = cardWidth * m_cardItems.size() * m_cardCoef + (1 - m_cardCoef) * cardWidth;
        int totalHeight = cardHeight;  // Assuming all cards are in a single row

        // Update the scene rect
        this->setSceneRect(0, 0, totalWidth, totalHeight);
    }
}
