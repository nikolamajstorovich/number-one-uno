#ifndef PLAYERITEM_H
#define PLAYERITEM_H

#include <QObject>
#include <QGraphicsScene>

class Card;
class CardItem;

class PlayerItem : public QGraphicsScene
{
    Q_OBJECT

public:
    PlayerItem( QObject *parent = nullptr);
    void updateCardPositions();
    int playerIndex();
    void setPlayerIndex(int playerIndex);
    void updateSceneRect();

    QVector<CardItem *> cardItems() const;

signals:
    void CardPressed(CardItem *item);

public slots:
    void newCardAdded(CardItem *card, int index);
    void RemoveCard(CardItem *item);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
    void NewItemPosition(CardItem *item);

    QString m_username;
    int m_playerIndex;

    QVector<CardItem *> m_cardItems;
    float m_cardCoef = 0.8;
};

#endif // PLAYERITEM_H
