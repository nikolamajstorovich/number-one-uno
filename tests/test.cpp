#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <QVariant>
#include <QString>
#include <QVector>
#include <QObject>
#include <QJsonObject>
#include <mainserver.h>
#include <unordered_set>

#include "card.h"
#include "player.h"
#include "dealerservice.h"
#include "game.h"
#include "gameworker.h"
#include "servercommunicationmanager.h"

TEST_CASE("Card::Card", "[constructor]")
{
    SECTION("A card object with no parametars should has it's color set on black and value on -1")
    {
        // Arrange
        Card card;

        // Act
        Card::COLOR color = card.color();
        int value = card.value();

        // Assert
        REQUIRE(color == Card::BLACK);
        REQUIRE(value == -1);
    }

    SECTION("A card consisting only of color and value is numeric card with those color and value")
    {
        // Arrange
        Card card(Card::RED, 5);

        // Act
        Card::COLOR color = card.color();
        Card::CARD_TYPE cardType = card.cardType();
        int value = card.value();

        // Assert
        REQUIRE(color == Card::RED);
        REQUIRE(cardType == Card::NUMERIC);
        REQUIRE(value == 5);
    }

    SECTION("A card consisting only of color and type can be either skip, revers or draw_two card in that color")
    {
        // Arrange
        Card card(Card::RED, Card::SKIP);

        // Act
        Card::COLOR color = card.color();
        Card::CARD_TYPE cardType = card.cardType();

        // Assert
        REQUIRE(color == Card::RED);
        REQUIRE(cardType == Card::SKIP);
    }

    SECTION("A card consisting only of type can be either wild card or draw_four")
    {
        // Arrange
        Card card(Card::WILD);

        // Act
        Card::CARD_TYPE cardType = card.cardType();

        // Assert
        REQUIRE(cardType == Card::WILD);
    }

    SECTION("A card that is created using another card must have the same attributes at that time")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(card1);

        // Act
        Card::COLOR color = card2.color();
        Card::CARD_TYPE cardType = card2.cardType();
        int value = card2.value();

        // Assert
        REQUIRE(color == Card::RED);
        REQUIRE(cardType == Card::NUMERIC);
        REQUIRE(value == 5);
    }
}

TEST_CASE("Card::color()", "[method] [getter]")
{
    SECTION("Color getter should return color stored in private member dedicated for color")
    {
        // Arrange
        Card::COLOR color = Card::RED;
        int value = 5;
        Card card(color, value);

        // Act
        Card::COLOR result = card.color();
    
        // Assert
        REQUIRE(result == color);
    }

    SECTION("For Wild card color getter should return black")
    {
        // Arrange
        Card card(Card::CARD_TYPE::WILD);

        // Act
        Card::COLOR expected = Card::BLACK;
        Card::COLOR result = card.color();

        // Assert
        REQUIRE(result == expected);
    }

    SECTION("For red revers card color getter should return red")
    {
        // Arrange
        Card::COLOR color = Card::RED;
        Card card(color, Card::CARD_TYPE::REVERSE);

        // Act
        Card::COLOR result = card.color();

        // Assert
        REQUIRE(result == color);
    }
}

TEST_CASE("Card::cardType()", "[method] [getter]")
{
    SECTION("Card type getter should return card type stored in private member dedicated for card type")
    {
        // Arrange
        Card::CARD_TYPE cardType = Card::NUMERIC;
        Card card(Card::RED, cardType);

        // Act
        Card::CARD_TYPE result = card.cardType();

        // Assert
        REQUIRE(result == cardType);
    }

    SECTION("For Wild card cardType getter should return wild")
    {
        // Arrange
        Card::CARD_TYPE cardType = Card::WILD;
        Card card(cardType);

        // Act
        Card::CARD_TYPE result = card.cardType();

        // Assert
        REQUIRE(result == cardType);
    }

    SECTION("For red revers card cardType getter should return reverse")
    {
        // Arrange
        Card::CARD_TYPE cardType = Card::REVERSE;
        Card card(Card::RED, cardType);

        // Act
        Card::CARD_TYPE result = card.cardType();

        // Assert
        REQUIRE(result == cardType);
    }
}

TEST_CASE("Card::value()", "[method] [getter]")
{
    SECTION("Value getter should return value stored in private member dedicated for value")
    {
        // Arrange
        Card::COLOR color = Card::RED;
        int value = 5;
        Card card(color, value);

        // Act
        int result = card.value();

        // Assert
        REQUIRE(result == value);
    }

    SECTION("For wild card value getter should return -1")
    {
        // Arrange
        Card card(Card::WILD);

        // Act
        int result = card.value();

        // Assert
        REQUIRE(result == -1);
    }
}

TEST_CASE("Card::operator ==", "[method]")
{
    SECTION("For cards with matched color and different types, method should return false")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::RED, Card::SKIP);

        // Act
        bool result = (card1 == card2);

        // Assert
        REQUIRE(result == false);
    }

    SECTION("For cards with matched color and types, but different values, method should return false")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::RED, 6);

        // Act
        bool result = (card1 == card2);

        // Assert
        REQUIRE(result == false);
    }

    SECTION("For cards with different colors, method should return false")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::PINK, 5);

        // Act
        bool result = (card1 == card2);

        // Assert
        REQUIRE(result == false);
    }

    SECTION("For cards with matched colors, types and values, method should return true")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::RED, 5);

        // Act
        bool result = (card1 == card2);

        // Assert
        REQUIRE(result == true);
    }
}

TEST_CASE("Card::operator !=", "[method]")
{
    SECTION("For cards with matched color and different types, method should return true")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::RED, Card::SKIP);

        // Act
        bool result = (card1 != card2);

        // Assert
        REQUIRE(result == true);
    }

    SECTION("For cards with matched color and types, but different values, method should return true")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::RED, 6);

        // Act
        bool result = (card1 != card2);

        // Assert
        REQUIRE(result == true);
    }

    SECTION("For cards with different colors, method should return true")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::PINK, 5);

        // Act
        bool result = (card1 != card2);

        // Assert
        REQUIRE(result == true);
    }
    SECTION("For cards with matched colors, types and values, method should return false")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::RED, 5);

        // Act
        bool result = (card1 != card2);

        // Assert
        REQUIRE(result == false);
    }
}

TEST_CASE("Card::operator =", "[method]")
{
    SECTION("An object made by using assignment operator have to has every data member matched with initial object")
    {
        // Arrange
        Card card1(Card::RED, 5);

        // Act
        Card card2 = card1;

        // Assert
        REQUIRE(card2.color() == card1.color());
        REQUIRE(card2.cardType() == card1.cardType());
        REQUIRE(card2.value() == card1.value());
    }
}

TEST_CASE("Card::isCompatibleWith", "[method]")
{
    SECTION("If values of cards are matched and color of first card is matched with color of the top of the pile, method returns true")
    {
        // Arrange
        Card card1(Card::RED, 5);
        Card card2(Card::PINK, 6);
        QString topPileColor = card1.colorName();

        // Act
        bool result = card1.isCompatibleWith(card2, topPileColor);

        // Assert
        REQUIRE(result == true);
    }
}

TEST_CASE("Card::toString", "[method]")
{
    SECTION("For red card of value 5, method returns [ RED - NUMERIC - 5 ]")
    {
        // Arrange
        Card card(Card::RED, 5);

        // Act
        std::string result = card.toString();

        // Assert
        REQUIRE(result == "[ RED - NUMERIC - 5 ]");
    }
}

TEST_CASE("Card::colorName", "[method]")
{
    SECTION("For red card method returns string RED")
    {
        // Arrange
        Card card(Card::RED, 5);

        // Act
        QString result = card.colorName();

        // Assert
        REQUIRE(result.toStdString() == "RED");
    }
}

TEST_CASE("Card::cardTypeName", "[method]")
{
    SECTION("For numeric card method returns string NUMERIC")
    {
        // Arrange
        Card card(Card::RED, Card::NUMERIC);

        // Act
        QString result = card.cardTypeName();

        // Assert
        REQUIRE(result.toStdString() == "NUMERIC");
    }
}

TEST_CASE("Card::toVariant", "[method]")
{
    SECTION("For red card of value 5 should return QVariant map {'color':'RED', 'cardType':'NUMERIC', 'value': 5}")
    {
        // Arrange
        Card card(Card::RED, 5);

        // Act
        QVariant result = card.toVariant();
        QVariantMap map = result.toMap();
        QString expectedColor = "RED";
        QString expectedCardType = "NUMERIC";
        int expectedValue = 5;

        // Assert
        REQUIRE(map["color"] == expectedColor);
        REQUIRE(map["cardType"] == expectedCardType);
        REQUIRE(map["value"] == expectedValue);
    }
}

TEST_CASE("Card::fromVariant", "[method]")
{
    SECTION("For received red card of value 5 in variant should set data members to red, numeric and five")
    {
        // Arrange
        Card card(Card::RED, 5);
        QVariant variant = card.toVariant();

        // Act
        card.fromVariant(variant);

        // Assert
        REQUIRE(card.color() == Card::RED);
        REQUIRE(card.cardType() == Card::NUMERIC);
        REQUIRE(card.value() == 5);
    }
}

TEST_CASE("Card::colorStrToEnum", "[method]")
{
    SECTION("For QString 'RED' method shoud return COLOR::RED")
    {
        // Arrange
        Card::COLOR expected = Card::RED;
        QString str = "RED";

        // Act
        Card::COLOR result = Card::colorStrToEnum(str);

        // Assert
        REQUIRE(result == expected);
    }
}

TEST_CASE("Card::cardTypeStrToEnum", "[method]")
{
    SECTION("For QString 'NUMERIC' method shoud return CARD_TYPE::NUMERIC")
    {
        // Arrange
        Card::CARD_TYPE expected = Card::NUMERIC;
        QString str = "NUMERIC";

        // Act
        Card::CARD_TYPE result = Card::cardTypeStrToEnum(str);

        // Assert
        REQUIRE(result == expected);
    }
}

// ##################################################################################################################################

TEST_CASE("Player::Player", "[constructor]")
{
    SECTION("At the creation, player named testUser has no cards and his unoChecked data member is set on false")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        
        // Act
        QString resultUsername = player.username();
        qsizetype numberofCards = player.numberOfCards();
        bool unoChecked = player.UnoChecked();

        // Assert
        REQUIRE(resultUsername == username);
        REQUIRE(numberofCards == 0);
        REQUIRE(unoChecked == false);
    }

}
TEST_CASE("Player::username()", "[method] [getter]")
{
    SECTION("Username getter should return username stored in private member dedicated for username")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);

        // Act
        QString result = player.username();

        // Assert
        REQUIRE(result == username);
    }
}


TEST_CASE("Player::numberOfCards()", "[method] [getter]")
{
    SECTION("Number of cards getter should return the size of the hand")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);
        player.pushCardToHand(card);

        // Act
        qsizetype result = player.numberOfCards();

        // Assert
        REQUIRE(result == 1);
    }
}

TEST_CASE("Player::hasOnlyOneCard()", "[method]")
{
    SECTION("Method should return true if the player has only one card")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);
        player.pushCardToHand(card);

        // Act
        bool result = player.hasOnlyOneCard();

        // Assert
        REQUIRE(result == true);
    }

    SECTION("Method should return false if the player has no cards")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);

        // Act
        bool result = player.hasOnlyOneCard();

        // Assert
        REQUIRE(result == false);
    }

    SECTION("Method should return false if the player has two cards")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card1(Card::RED, 5);
        Card card2(Card::WILD);
        player.pushCardToHand(card1);
        player.pushCardToHand(card2);

        // Act
        bool result = player.hasOnlyOneCard();

        // Assert
        REQUIRE(result == false);
    }
}

TEST_CASE("Player::hasNoCards()", "[method]")
{
    SECTION("Method should return true if the player has no cards")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);

        // Act
        bool result = player.hasNoCards();

        // Assert
        REQUIRE(result == true);
    }

    SECTION("Method should return false if the player has one card")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);
        player.pushCardToHand(card);

        // Act
        bool result = player.hasNoCards();

        // Assert
        REQUIRE(result == false);
    }
}

TEST_CASE("Player::pushCardToHand()", "[method]")
{
    SECTION("If card is added to player's hand, number of cards in hand should be increased by one")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);
        qsizetype initialSize = player.numberOfCards();

        // Act
        player.pushCardToHand(card);
        qsizetype finalSize = player.numberOfCards();

        // Assert
        REQUIRE(finalSize == initialSize + 1);
    }

    SECTION("Card added to player's hand is stored at the end of his hand")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);

        // Act
        player.pushCardToHand(card);
        Card result = player.peekCard();

        // Assert
        REQUIRE(result == card);
    }
}

TEST_CASE("Player::peekCard()", "[method]")
{
    SECTION("Method should return the last card in the player's hand without removing it")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);
        player.pushCardToHand(card);

        // Act
        Card result = player.peekCard();

        // Assert
        REQUIRE(result == card);
    }
}

TEST_CASE("Player::peekCard(unsigned int index)", "[method]")
{
    SECTION("Method should return the card on specified index in the player's hand without removing it")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card1(Card::RED, 5);
        Card card2(Card::PINK, 3);
        player.pushCardToHand(card1);
        player.pushCardToHand(card2);
        unsigned int index = 0;

        // Act
        Card result = player.peekCard(index);

        // Assert
        REQUIRE(result == card1);
    }
}

TEST_CASE("Player::throwCard()", "[method]")
{
    SECTION("Method should remove a card from the player's hand and return it")
    {
        // Arrange
        QString username = "testUser";
        Player player(username);
        Card card(Card::RED, 5);
        player.pushCardToHand(card);
        qsizetype initialSize = player.numberOfCards();

        // Act
        Card result = player.throwCard(0);
        qsizetype finalSize = player.numberOfCards();

        // Assert
        REQUIRE(result == card);
        REQUIRE(finalSize == initialSize - 1);
    }
}

// #######################################################################################################################################


TEST_CASE("Game::executeMSG()","[method]"){
    SECTION("Method executeMSG() should add a card on empty hand"){
        Game game;
        game.start(QVector<QString>{"Aca"});

        QJsonObject obj;
        obj.insert("type", "deckClicked");
        obj.insert("playerIndex",0);

        REQUIRE_NOTHROW(game.executeMSG(obj));
        QVector<Player> players=game.Players();

        REQUIRE(8==players[0].numberOfCards());
    }
}

TEST_CASE("DealerService.deal()","[method]"){
    SECTION("Test takeCard() function") {
        DealerService dealer=DealerService();
        // Testiranje takeCard() funkcije
            // Testiranje discard() funkcije
        Card card(Card::PINK, 8); // Kreiranje karte za testiranje
        dealer.discard(card);
        REQUIRE(dealer.getTopCardColor() == "PINK"); // Provera da li je boja karte postavljena kao trenutna boja na vrhu špila

    }

    SECTION("Test shuffle() function") {
        DealerService dealer=DealerService();
        // Testiranje shuffle() funkcije
        DealerService dealers=dealer;
        dealers.shuffle();

        REQUIRE(sizeof(dealer) == sizeof(dealers));
    }

    SECTION("Test getTopCardColor() and setTopCardColor() functions") {
        DealerService dealer=DealerService();
        // Testiranje getTopCardColor() i setTopCardColor() funkcija
        dealer.setTopCardColor("RED");
        REQUIRE(dealer.getTopCardColor() == "RED"); // Provera postavljene boje na vrhu špila
    }

}

TEST_CASE("DealerService::topCard()", "[method]")
{
    SECTION("Test that topCard() returns card on top of pile (Last card we discarded)") {
    DealerService dealer{};
    Card card(Card::COLOR::RED, 2);

    dealer.discard(card);

    REQUIRE(dealer.topCard() == card);
    }
}

// TEST_CASE("DealerService::takeCard()", "[method]")
// {
//     SECTION("Test that take() adds card on top of deck (We check by taking card from top of deck)") {
//         DealerService dealer{};
//         Card card(Card::COLOR::RED, 2);

//         dealer.takeCard(card);

//         REQUIRE(dealer.deal() == card);
//     }
// }

TEST_CASE("DealerService::discard()", "[method]")
{
    SECTION("Test that discard() adds card on top of pile") {
        DealerService dealer{};
        Card card(Card::COLOR::RED, 2);

        dealer.discard(card);

        REQUIRE(dealer.topCard() == card);
    }
}

TEST_CASE("DealerService::deal()", "[method]")
{
    SECTION("Test dealing a card from a non-empty deck") {
        DealerService dealer{};
        Card card;

        REQUIRE(dealer.deal(card));
        REQUIRE_FALSE(dealer.topCard() == card);
    }

    SECTION("Test dealing a card from an empty deck triggers refill") {
        DealerService dealer{};
        Card card;

        // Empty the deck
        while (dealer.deal(card))
            ;

        REQUIRE(dealer.deal(card) == false);
        REQUIRE_FALSE(dealer.topCard() == card);
    }
}

TEST_CASE("DealerService::takeCard()", "[method]")
{
    SECTION("Test taking a card adds it back to the deck") {
        DealerService dealer;
        Card card;

        dealer.deal(card); // Ensure the deck is not empty
        dealer.discard(card);

        REQUIRE(dealer.topCard() == card);
    }
}

TEST_CASE("DealerService::shuffle()", "[method]")
{
    SECTION("Test shuffling the deck changes the order") {
        DealerService dealer{};
        Card originalTopCard = dealer.topCard();

        dealer.shuffle();

        REQUIRE_FALSE(dealer.topCard() == originalTopCard);
    }
}

TEST_CASE("DealerService::refill()", "[method]")
{
    SECTION("Test refilling the deck after dealing all cards") {
        DealerService dealer{};
        Card card;

        // Empty the deck
        while (dealer.deal(card))
            ;

        dealer.refill();

        REQUIRE_FALSE(dealer.topCard() == card);
    }
}

TEST_CASE("Pile.isEmpty()","[method]"){
    SECTION("Testing isEmpty()") {
        Pile pile;
        REQUIRE(pile.isEmpty() == true);

    }

    SECTION("Numofcards"){
        Pile pile;
        Card card(Card::PINK, 8);
        pile.addCard(card);
        REQUIRE(pile.isEmpty()==false);
    }

    SECTION("card=card"){
        Pile pile;
        Card card(Card::PINK, 8);
        pile.addCard(card);
        Card topcard=pile.topCard();
        REQUIRE(topcard==card);
    }

    SECTION("card=card"){
        Pile pile;
        Card card(Card::PINK, 8);
        pile.addCard(card);
        Card topcard=pile.giveCard();
        REQUIRE(topcard==card);
    }

    SECTION("card=card"){
        Pile pile;
        Card card(Card::PINK, 8);
        pile.addCard(card);
        Card topcard=pile.giveCard();
        REQUIRE(pile.isEmpty()==true);
    }

    SECTION("Testing topCard()") {
        Pile pile;

        Card card1(Card::PINK, 8);
        pile.addCard(card1);

        REQUIRE(pile.topCard()== card1);
    }

    SECTION("Testing topCard()") {
        Pile pile;

        Card card1(Card::PINK, 8);
        pile.addCard(card1);

        REQUIRE(pile.topCard().value()==8);
    }
}

// TEST_CASE("Deck::giveCard()", "[method]")
// {
//     SECTION("The card given should be the same as the top card before giving")
//     {
//         // Arrange
//         Deck deck;
//         Card card(Card::RED, 5);
//         deck.addCard(card);

//         // Act
//         Card& topCardBefore = deck.topCard();
//         Card& givenCard = deck.giveCard();

//         // Assert
//         REQUIRE(givenCard == topCardBefore);
//     }
// }

TEST_CASE("Deck::addCard(Card& card)", "[method]")
{
    SECTION("Added card should be stored on the top of the deck")
    {
        // Arrange
        Deck deck;
        Card card(Card::RED, 5);


        // Act
        deck.addCard(card);
        Card topCard = deck.topCard();

        // Assert
        REQUIRE(card == topCard);
    }
}

TEST_CASE("Deck::isEmpty()", "[method]")
{
    SECTION("isEmpty should return true if the deck has no cards")
    {
        // Arrange
        Deck deck;

        // Act
        bool isEmpty = deck.isEmpty();

        // Assert
        REQUIRE(isEmpty == true);
    }

    SECTION("isEmpty should return false if the deck has one card")
    {
        // Arrange
        Deck deck;
        Card card(Card::RED, 5);
        deck.addCard(card);

        // Act
        bool isEmpty = deck.isEmpty();

        // Assert
        REQUIRE(isEmpty == false);
    }
}

TEST_CASE("Testing GameWorker class methods2", "[GameWorker]")
{
    GameWorker gameWorker("TestGame");
    gameWorker.addPlayerToLobby("Player1", 5);
    gameWorker.addPlayerToLobby("Player2", 6);

    SECTION("Testing gameWorker()")
    {
        REQUIRE(gameWorker.getGameID() == "TestGame");
    }

    SECTION("addplayer")
    {
        REQUIRE(gameWorker.numOfPlayers() == 2);
        gameWorker.addPlayerToLobby("Player3", 7);
        REQUIRE(gameWorker.numOfPlayers() == 3);
    }

    SECTION("removeplayer")
    {
        REQUIRE(gameWorker.numOfPlayers() == 2);
        gameWorker.removePlayerFromLobby(6);
        REQUIRE(gameWorker.numOfPlayers() == 1);
    }

    SECTION("isAdmin")
    {
        REQUIRE(gameWorker.isAdmin(5) == true);
        REQUIRE(gameWorker.isAdmin(6) == false);
    }

    SECTION("Existing username returns true")
    {
        REQUIRE(gameWorker.usernameExists("Player1") == true);
        REQUIRE(gameWorker.usernameExists("Player2") == true);
    }

    SECTION("Non-existing username returns false")
    {
        REQUIRE(gameWorker.usernameExists("NonExistingPlayer") == false);
    }

    SECTION("startGame initializes the game")
    {
        REQUIRE(gameWorker.isInProgress() == false);
        gameWorker.startGame();
        REQUIRE(gameWorker.isInProgress() == true);
    }

    SECTION("isInProgress returns false when game is not started")
    {
        REQUIRE(gameWorker.isInProgress() == false);
    }

    SECTION("broadcast sends JSON to all players")
    {
        QJsonObject testJson = {
            {"type", "testType"},
            {"message", "Hello, players!"}
        };

        int signalCounter = 0;
        QObject::connect(&gameWorker, &GameWorker::sendJsonSignal, [&](qintptr socket, const QJsonObject &json) {
            signalCounter++;
            REQUIRE(json == testJson);
        });

        gameWorker.broadcast(testJson);
        REQUIRE(signalCounter == gameWorker.numOfPlayers());
    }

    SECTION("Player leaving in progress triggers broadcast and ends the game")
    {
        gameWorker.startGame();

        qintptr leavingSocket = 5;

        int signalCounter = 0;
        QObject::connect(&gameWorker, &GameWorker::sendJsonSignal, [&](qintptr socket, const QJsonObject &json) {
            signalCounter++;

            REQUIRE(json["type"] == "error");
            REQUIRE(json["text"] == QString("Player1 left :("));
        });

        gameWorker.playerLeftWhileGameInProgress(leavingSocket);

        REQUIRE(signalCounter == 1);
        REQUIRE(gameWorker.isInProgress() == false);
    }

    SECTION("endGame deletes the game and emits gameEnded signal")
    {
        gameWorker.startGame();

        int signalCounter = 0;
        QObject::connect(&gameWorker, &GameWorker::gameEnded, [&](const QString &gameID) {
            signalCounter++;
            REQUIRE(gameID == "TestGame");
        });

        gameWorker.endGame();

        REQUIRE(signalCounter == 1);
        REQUIRE(gameWorker.isInProgress() == false);
    }
}


TEST_CASE("connectToServer function", "[ServerCommunicationManager]") {

    MainServer server;
    int port = 1967;
    server.listen(QHostAddress::Any, port);

    ServerCommunicationManager& communicationManager = ServerCommunicationManager::instance();

    SECTION("Successful connection")
    {
        QString serverAddress = "127.0.0.1";
        int serverPort = 1967;

        bool connected = communicationManager.connectToServer(serverAddress, serverPort);

        REQUIRE(connected == true);
        REQUIRE(communicationManager.isConnected() == true);
    }

    SECTION("Unsuccessful connection")
    {
        QString invalidServerAddress = "127.0.0.1";
        int invalidServerPort = 9999;

        bool connected = communicationManager.connectToServer(invalidServerAddress, invalidServerPort);

        REQUIRE(connected == false);
        REQUIRE(communicationManager.isConnected() == false);
    }

}

