#include "game.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

Game::Game(QObject *parent)
    : QObject(parent)
    , m_ClockwiseOrientation(false)
    , m_Uno(false)
    , m_DealerService(DealerService())
    , m_CurrentPlayer(0)
{}

void Game::addPlayer(const QString &username)
{
    m_Players.emplaceBack(Player{username});
}

void Game::start(const QVector<QString> &usernames)
{
    for(const auto &username : usernames)
        addPlayer(username);

    playCardFromDeck();
    for(int j = 0; j < 7; ++j){
        for(int i = 0; i < m_Players.size(); ++i) {
            drawCard(i, 1);
        }
    }
}

void Game::executeMSG(const QJsonObject &obj)
{
    QVariantMap msg = obj.toVariantMap();
    if(msg.value("type").toString() == "msg"){
        qDebug() << msg.value("playerIndex").toInt();
        msg_chat(msg.value("playerIndex").toInt(),msg.value("text").toString());
        return;
    }

    if(msg.value("type").toString() == "deckClicked") {
        drawCard(msg.value("playerIndex").toInt(), 1);
        return;
    }
    if (msg.value("type").toString() == "cardClicked") {
        playCard(msg.value("playerIndex").toInt(), msg.value("handCardIndex").toInt());
        return;
    }
    if (msg.value("type").toString() == "dialogWildCard") {
        changeColor(msg.value("color").toString());
        return;
    }
    if (msg.value("type").toString() == "unoClicked") {
        unoCheck(msg.value("playerIndex").toInt());
        return;
    }
    if (msg.value("type").toString() == "endTurnClicked") {
        nextPlayer();
        QVariantMap msg;
        msg.insert("type", "turnEnded");
        msg.insert("currentPlayer", m_CurrentPlayer);
        emit jsonContent(QJsonObject::fromVariantMap(msg));
        return;
    }
}

void Game::msg_chat(unsigned playerIndex,const QString &msgstring)
{
    QVariantMap msg;
    msg.insert("type","msg");
    msg.insert("playerIndex",playerIndex);
    msg.insert("text",msgstring);
    emit jsonContent(QJsonObject::fromVariantMap(msg));
}

void Game::playCardFromDeck()
{
    Card card;
    if(!m_DealerService.deal(card)) {
        return;
    }
    m_DealerService.discard(card);
    while(m_DealerService.topCard().cardType() != Card::CARD_TYPE::NUMERIC) {
        m_DealerService.refill();
        if(!m_DealerService.deal(card))
            return;
        m_DealerService.discard(card);
    }

    QVariantMap msg;
    msg.insert("type", "firstCardPlayed");
    msg.insert("card", m_DealerService.topCard().toVariant());
    emit jsonContent(QJsonObject::fromVariantMap(msg));
}


void Game::playCard(unsigned playerIndex,unsigned cardIndex)
{
    // If card can be played, remove it from players hand and add it to the top of the pile

    if( m_Players[playerIndex].peekCard(cardIndex).isCompatibleWith(m_DealerService.topCard(), m_DealerService.getTopCardColor()) )
    {
        m_DealerService.discard(m_Players[playerIndex].throwCard(cardIndex));

        QVariantMap msg;
        msg.insert("type", "cardPlayed");
        msg.insert("playerIndex", playerIndex);
        msg.insert("card", m_DealerService.topCard().toVariant());
        msg.insert("cardIndex", cardIndex);

        emit jsonContent(QJsonObject::fromVariantMap(msg));

        if(m_Players[playerIndex].hasNoCards())
        {
            weHaveWinner = true;
            QVariantMap endgameMsg;
            endgameMsg.insert("type", "gameEnded");
            endgameMsg.insert("winner", playerIndex);

            emit jsonContent(QJsonObject::fromVariantMap(endgameMsg));
            emit gameEnded();
        }

        cardPostEffects(m_DealerService.topCard(), playerIndex);
    }
}

void Game::drawCard(unsigned playerIndex, unsigned numOfCards)
{
    while(numOfCards--)
    {
        Card card;
        if (!m_DealerService.deal(card)) {
            return;
        }
        m_Players[playerIndex].pushCardToHand(card);
        m_Players[playerIndex].setUnoChecked(false);

        QVariantMap msg;
        msg.insert("type", "cardDealt");
        msg.insert("playerIndex", playerIndex);
        msg.insert("card", card.toVariant());
        emit jsonContent(QJsonObject::fromVariantMap(msg));
    }

}

void Game::reverseOrientation()
{
    m_ClockwiseOrientation = !m_ClockwiseOrientation;
    QVariantMap msg;
    msg.insert("type", "orientationChanged");
    emit jsonContent(QJsonObject::fromVariantMap(msg));
}

void Game::skipPlayer()
{
    nextPlayer();
    QVariantMap msg;
    msg.insert("type", "skipPlayer");
    emit jsonContent(QJsonObject::fromVariantMap(msg));
}

// set chosenColor as a color of the top of the pile
void Game::changeColor(const QString &chosenColor)
{
    m_DealerService.setTopCardColor(chosenColor);
    QVariantMap msg;
    msg.insert("type", "colorChanged");
    msg.insert("color", chosenColor);
    emit jsonContent(QJsonObject::fromVariantMap(msg));
    endTurn();
}

void Game::nextPlayer()
{
    m_CurrentPlayer = indexOfNextPlayer();
}

void Game::unoCheck(int playerIndex)
{
    bool uno = false;
    for(auto & m_Player : m_Players)
        if(m_Player.hasOnlyOneCard()) {
            uno = true;
            break;
        }

    if(!uno) {
        drawCard(playerIndex, 2);
        return;
    }

    if(m_Players[playerIndex].hasOnlyOneCard()) {
        m_Players[playerIndex].setUnoChecked(true);
        QVariantMap msg;
        msg.insert("type", "calledUno");
        msg.insert("playerIndex", playerIndex);
        emit jsonContent(QJsonObject::fromVariantMap(msg));
    }

    for(int i = 0; i < m_Players.size(); ++i)
        if(m_Players[i].hasOnlyOneCard() && !m_Players[playerIndex].UnoChecked()) {
            drawCard(i, 2);
        }
}

void Game::cardPostEffects(const Card &card, const int playerIndex)
{
    if(card.cardType() == Card::CARD_TYPE::NUMERIC) {
        endTurn();
        return;
    }

    if(card.cardType() == Card::CARD_TYPE::SKIP) {
        skipPlayer();
        endTurn();
        return;
    }

    if(card.cardType() == Card::CARD_TYPE::REVERSE) {
        reverseOrientation();
        endTurn();
        return;
    }

    if(card.cardType() == Card::CARD_TYPE::DRAW_TWO) {
        drawCard(indexOfNextPlayer(), 2);
        endTurn();
        return;
    }

    if(card.cardType() == Card::CARD_TYPE::WILD) {
        QVariantMap msg;
        msg.insert("type", "chooseColor");
        msg.insert("playerIndex", playerIndex);
        emit jsonContent(QJsonObject::fromVariantMap(msg));
        return;
    }

    if(card.cardType() == Card::CARD_TYPE::WILD_DRAW_FOUR) {

        drawCard(indexOfNextPlayer(), 4);
        skipPlayer();
        QVariantMap msg;
        msg.insert("type", "chooseColor");
        msg.insert("playerIndex", playerIndex);
        emit jsonContent(QJsonObject::fromVariantMap(msg));
        return;
    }
}

void Game::endTurn()
{
    nextPlayer();
    QVariantMap msg;
    msg.insert("type", "turnEnded");
    msg.insert("currentPlayer", m_CurrentPlayer);
    emit jsonContent(QJsonObject::fromVariantMap(msg));
}

// auxiliary function that returns index of next player
// deppending on orientation and current player
auto Game::indexOfNextPlayer() const -> int
{
    int index = m_CurrentPlayer;
    index += m_ClockwiseOrientation ? 1 : -1;
    index = (index + m_Players.size()) % m_Players.size();
    return index;
}

auto Game::Players() const -> QVector<Player>
{
    return m_Players;
}
