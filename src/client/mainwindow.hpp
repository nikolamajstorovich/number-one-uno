#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QWidget>
#include <QStackedWidget>
#include <QDialog>
#include "gamewindow.h"
#include <QMediaPlayer>
#include <QAudioOutput>
#include <QUrl>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    QString m_username;

    // Enum for pages in the QStackedWidget
    enum class Page {
        Menu,
        Preferences,
        CreateAGame,
        JoinAGame,
        Lobby,
        GameRules
    };

    void setCurrentPage(Page page);

    ~MainWindow();

    void setImgPath(QString newImgPath);

    QString getImgPath() const;

private slots:
    void on_pbBackPreferences_clicked();

    void on_pbPreferences_clicked();

    void on_pbCreateAGame_clicked();

    void on_pbJoinAGame_clicked();

    void on_pbBack_cg_clicked();

    void on_pbBack_jg_clicked();

    void on_pbLobby_jg_clicked();

    void on_pbLobby_cg_clicked();

    void on_pbStartGame_lb_clicked();

    void goToStartScreen();

    void goToLobby(QString gameID, QVector<QString> players);

    void joinLobby(QString gameID, QVector<QString> players);

    void startGame(QVector<QString> players);

    void on_pbBackground1_clicked();

    void on_pbBackground2_clicked();

    void on_pbBackground3_clicked();

    void on_volumeSlider_valueChanged(int value);

    void on_pbBack_gr_clicked();

    void on_pbGameRules_clicked();

private:

    Ui::MainWindow *ui;
    GameWindow* gameWindow;
    QDialog* dialogWarning;
    QString m_imgPath = "../../resources/images/gamewindow_background.jpg";

    void createDialogWarning(QString message);

    QMediaPlayer *mediaPlayer;
    QAudioOutput *audioOutput;

};
#endif // MAINWINDOW_HPP
