#include "deck.h"

Deck::Deck()
    : m_Cards(QVector<Card>())
{}

void Deck::shuffle()
{
    std::shuffle(std::begin(m_Cards), std::end(m_Cards), std::default_random_engine{seed});
}

auto Deck::topCard() -> Card
{
    return m_Cards.back();
}

auto Deck::giveCard() -> Card
{
    Card card = m_Cards.last();
    m_Cards.pop_back();
    return card;
}

void Deck::addCard(Card card)
{
    m_Cards.push_back(card);
}

auto Deck::isEmpty() -> bool
{
    return m_Cards.empty();
}

