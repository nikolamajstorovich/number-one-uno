#ifndef CARD_H
#define CARD_H

#include <QVariant>
#include <QString>
#include <iostream>
#include <string>
#include <stdexcept>

class Card
{
public:
    enum COLOR { RED, PINK, TURQUOISE, PURPLE, BLACK };
    enum CARD_TYPE { NUMERIC, SKIP, REVERSE, DRAW_TWO, WILD, WILD_DRAW_FOUR };

    Card();
    ~Card();

    // NUMERIC
    Card(COLOR color, int value);

    // SKIP, REVERSE, DRAW_TWO
    Card(COLOR color, CARD_TYPE cardType);

    // WILD, WILD_DRAW_FOUR
    Card(CARD_TYPE cardType);

    Card(const Card &other);

    COLOR color() const;
    CARD_TYPE cardType() const;
    int value() const;

    bool operator ==(const Card &other) const;
    bool operator !=(const Card &other) const;
    Card& operator =(const Card &other);

    // check if this card can be played on th top of other card
    bool isCompatibleWith(const Card& other, const QString &topCardColor) const;

    // convert enums to string
    QString colorName() const;
    QString cardTypeName() const;

    std::string toString() const;

    // serialization
    QVariant toVariant() const;
    void fromVariant(const QVariant& variant);

    static COLOR colorStrToEnum(const QString &str);
    static CARD_TYPE cardTypeStrToEnum(const QString &str);

private:
    COLOR m_color = BLACK;
    CARD_TYPE m_cardType;
    int m_value = -1;
};

std::ostream &operator<<(std::ostream &ostr, const Card &card);

#endif // CARD_H
