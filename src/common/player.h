#ifndef PLAYER_H
#define PLAYER_H

#include "card.h"
#include <QString>
#include <QVector>

class Player
{
public:
    Player(const QString &username);

    QString username() const;
    QVector<Card> hand() const;
    unsigned int score() const;

    bool hasOnlyOneCard() const;
    bool hasNoCards() const;
    qsizetype numberOfCards() const;

    void pushCardToHand(const Card &card);
    Card peekCard(unsigned int index) const;
    Card peekCard() const;
    Card throwCard(unsigned int index);
    
    bool UnoChecked() const;
    void setUnoChecked(bool newUnoChecked);

private:
    QString m_username;
    QVector<Card> m_hand;
    bool m_UnoChecked = false;
};

#endif // PLAYER_H
