#ifndef PILE_H
#define PILE_H

#include <vector>
#include "card.h"
#include <QString>

class Pile
{
public:
    Pile();
    Card topCard();
    Card giveCard();
    void addCard(Card& card);
    bool isEmpty();
    QString getTopCardColor() const;
    void setTopCardColor(const QString &newTopCardColor);

private:
    QVector<Card> m_Cards;
    QString topCardColor;

};

#endif // PILE_H
