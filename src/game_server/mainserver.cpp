#include "gameworker.h"
#include "mainserver.h"

#include <QThread>
#include <QDataStream>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QRandomGenerator>

MainServer::MainServer(QObject *parent)
    : QTcpServer(parent)
    , m_idealThreadCount(qMax(QThread::idealThreadCount(), 1))
{
    m_availableThreads.reserve(m_idealThreadCount);
}

MainServer::~MainServer()
{
    const QJsonObject json = {
        {"type", "error"},
        {"text", "Server shut down."},
    };
    for(qintptr socketDesc : socketMap.keys())
    {
        sendJson(socketDesc, json);
    }

    for (QThread *singleThread : m_availableThreads)
    {
        singleThread->quit();
        singleThread->wait();
        delete singleThread;
    }
    m_availableThreads.clear();
}

void MainServer::incomingConnection(qintptr socketDescriptor)
{
    auto *socket = new QTcpSocket();

    if (!socket->setSocketDescriptor(socketDescriptor)) {
        socket->deleteLater();
        return;
    }
    socketMap[socketDescriptor] = socket;
    connect(socket, &QTcpSocket::readyRead, this, &MainServer::receiveJson);
    connect(socket, &QTcpSocket::disconnected, this, &MainServer::onDisconnected);

    qDebug() << QStringLiteral("New client Connected");
}

void MainServer::createGame(QString username, QTcpSocket* socket)
{
    int threadIdx = m_availableThreads.size();
    if (threadIdx >= m_idealThreadCount)
    {
        const QJsonObject json = {
            {"type", "error"},
            {"text", "Servers are full"},
        };
        sendJson(socket->socketDescriptor(), json);
        return;
    }

    auto *game = new GameWorker(generateUniqueGameId());
    connect(game, &GameWorker::sendJsonSignal, this, &MainServer::sendJson);
    connect(game, &GameWorker::gameEnded, this, &MainServer::destroyGame);

    m_availableThreads.append(new QThread(this));
    m_availableThreads.last()->start();
    game->moveToThread(m_availableThreads.at(threadIdx));
    workerMap[game->getGameID()] = game;

    game->addPlayerToLobby(username, socket->socketDescriptor());
    gameIDBySocket[socket] = game->getGameID();
}

void MainServer::joinGame(QString username, QTcpSocket* socket, QString gameID)
{
    if (!gameExists(gameID))
    {
        QJsonObject json = {
            {"type", "error"},
            {"text", QString("Game %1 doesn't exist").arg(gameID)},
        };
        sendJson(socket->socketDescriptor(), json);
        return;
    }

    GameWorker* game = workerMap[gameID];

    if (game->usernameExists(username))
    {
        QJsonObject json = {
            {"type", "error"},
            {"text", "That username is taken. Try another."},
        };
        sendJson(socket->socketDescriptor(), json);
        return;
    }

    if (game->numOfPlayers() >= 4)
    {
        QJsonObject json = {
            {"type", "error"},
            {"text", "Game is full."},
        };
        sendJson(socket->socketDescriptor(), json);
        return;
    }

    game->addPlayerToLobby(username, socket->socketDescriptor());
    gameIDBySocket[socket] = gameID;
}

void MainServer::destroyGame(QString gameID)
{

    // no longer assocate those sockets with that gameID
    for (auto it = gameIDBySocket.constBegin(); it != gameIDBySocket.constEnd();)
    {
        if (it.value() == gameID) {
            it = gameIDBySocket.erase(it);
        } else {
            ++it;
        }
    }

    m_availableThreads.remove(m_availableThreads.indexOf(workerMap[gameID]->thread()));
    workerMap[gameID]->thread()->quit();
    workerMap[gameID]->deleteLater();
    workerMap.remove(gameID);
}

auto MainServer::gameExists(QString gameID) -> bool
{
    return workerMap.contains(gameID);
}

void MainServer::receiveJson()
{
    QByteArray jsonData;
    auto *socket = qobject_cast<QTcpSocket *>(sender());

    QDataStream socketStream(socket);
    for (;;) {
        socketStream.startTransaction();
        socketStream >> jsonData;
        if (socketStream.commitTransaction()) {
            QJsonParseError parseError;
            const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
            if (parseError.error == QJsonParseError::NoError) {
                if (jsonDoc.isObject())
                    parseJson(socket, jsonDoc.object());
                else
                    qDebug() << QLatin1String("Invalid message: ") + QString::fromUtf8(jsonData);
            } else {
                qDebug() <<  QLatin1String("Invalid message: ") + QString::fromUtf8(jsonData);
            }
        } else {
            break;
        }
    }
}

void MainServer::parseJson(QTcpSocket* sender, const QJsonObject &json)
{
    Q_ASSERT(sender);

    const QJsonValue typeVal = json.value(QLatin1String("type"));
    if (typeVal.isNull() || !typeVal.isString())
        return;

    const QString type = typeVal.toString();


    if (type == "createGame" && json.contains("username")) {
        createGame(json["username"].toString(), sender);
        return;
    }
    if (type == "joinGame" && json.contains("username") && json.contains("gameID")) {
        joinGame(json["username"].toString(), sender, json["gameID"].toString().toUpper());
        return;
    }

    QString gameID;
    if (gameIDBySocket.contains(sender)){
        gameID = gameIDBySocket[sender];
    }else{
        return;
    }

    if (type == "startGame") {
        QMetaObject::invokeMethod(workerMap[gameID], "startGame", Qt::QueuedConnection);
        return;
    }

    QMetaObject::invokeMethod(workerMap[gameID], "receiveJson", Qt::QueuedConnection, Q_ARG(const QJsonObject&, json));
}

void MainServer::sendJson(qintptr socketDesc, const QJsonObject &json)
{
    QDataStream socketStream(socketMap[socketDesc]);
    socketStream << json;
}

void MainServer::onDisconnected()
{
    auto *socket = qobject_cast<QTcpSocket *>(sender());

    if (!socket)
        return;

    if (!gameIDBySocket.contains(socket))
    {
        for (auto it = socketMap.constBegin(); it != socketMap.constEnd();) {
            if (it.value() == socket) {
                it = socketMap.erase(it);
            } else {
                ++it;
            }
        }
        socket->deleteLater();
        return;
    }

    QString gameID = gameIDBySocket[socket];
    GameWorker* game = workerMap[gameID];
    qintptr socketDesc = -1;
    for (auto it = socketMap.constBegin(); it != socketMap.constEnd(); it++)
    {
        if (it.value() == socket) { socketDesc = it.key(); }
    }


    if (game->isInProgress())
    {
        game->playerLeftWhileGameInProgress(socketDesc);
    }
    else
    {
        bool wasAdmin = game->isAdmin(socketDesc);

        game->removePlayerFromLobby(socketDesc);
        gameIDBySocket.remove(socket);

        if (wasAdmin)
        {
            const QJsonObject json = {
                {"type", "goToStartScreen"},
                {"text", "Admin left!"},
            };
            sendJson(socket->socketDescriptor(), json);
            game->broadcast(json);
            destroyGame(gameID);
        }
    }

    socketMap.remove(socketDesc);
    socket->deleteLater();
    return;
}

auto MainServer::generateUniqueGameId() -> QString
{
    QString newGameId;

    do {
        newGameId = generateRandom4LetterCombination();
    } while (gameExists(newGameId));

    return newGameId;
}

auto MainServer::generateRandom4LetterCombination() -> QString
{
    const QString alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    QString result;

    for (int i = 0; i < 4; ++i)
    {
        result += alphabet.at(QRandomGenerator::global()->generate() % alphabet.length());
    }

    return result;
}
