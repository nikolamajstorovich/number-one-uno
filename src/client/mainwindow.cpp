#include "mainwindow.hpp"
#include "./ui_mainwindow.h"
#include "gamewindow.h"
#include "servercommunicationmanager.h"

#include <QJsonObject>
#include <QStyle>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("Number one UNO");

    //Background and buttons

    QPixmap bkgnd("../../resources/images/main_menu_background.jpeg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    //Music
    mediaPlayer = new QMediaPlayer(this);
    audioOutput = new QAudioOutput(this);
    mediaPlayer->setAudioOutput(audioOutput);
    mediaPlayer->setSource(QUrl::fromLocalFile("../../resources/music/UNO_music.mp3"));
    audioOutput->setVolume(50);
    mediaPlayer->play();

    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::goToLobby, this, &MainWindow::goToLobby);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::joinLobby, this, &MainWindow::joinLobby);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::startGameSignal, this, &MainWindow::startGame);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::showError, this, &MainWindow::createDialogWarning);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::goToStartScreenSignal, this, &MainWindow::goToStartScreen);


    connect(ui->volumeSlider, &QSlider::valueChanged, this, &MainWindow::on_volumeSlider_valueChanged);
    connect(ui->volumeSlider_2, &QSlider::valueChanged, this, &MainWindow::on_volumeSlider_valueChanged);
    connect(ui->volumeSlider_3, &QSlider::valueChanged, this, &MainWindow::on_volumeSlider_valueChanged);
    connect(ui->volumeSlider_4, &QSlider::valueChanged, this, &MainWindow::on_volumeSlider_valueChanged);
    connect(ui->volumeSlider_5, &QSlider::valueChanged, this, &MainWindow::on_volumeSlider_valueChanged);

    // Set default page
    setCurrentPage(Page::Menu);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mediaPlayer;
    delete audioOutput;
}


void MainWindow::on_pbPreferences_clicked()
{
    setCurrentPage(Page::Preferences);
}

void MainWindow::on_pbCreateAGame_clicked()
{
    ui->pbStartGame_lb->show();
    setCurrentPage(Page::CreateAGame);
}

void MainWindow::on_pbJoinAGame_clicked()
{
    setCurrentPage(Page::JoinAGame);
}

void MainWindow::on_pbBackPreferences_clicked()
{
    setCurrentPage(Page::Menu);
}

void MainWindow::on_pbBack_cg_clicked()
{

    setCurrentPage(Page::Menu);
}

void MainWindow::on_pbBack_jg_clicked()
{
    setCurrentPage(Page::Menu);
}

void MainWindow::setCurrentPage(Page page)
{
    ui->stackedWidget->setCurrentIndex(static_cast<int>(page));
}


void MainWindow::on_pbLobby_jg_clicked()
{
    QString username = ui->leUsername_jg->text();
    if (username.isEmpty()){
        createDialogWarning("Please enter username");
    }
    else{
        m_username = username;
        QString gameID = ui->leEnterId_jg->text();
        ServerCommunicationManager::instance().joinGame(username, gameID);
    }
}


void MainWindow::on_pbLobby_cg_clicked()
{
    QString username =  ui->leUsername->text();
    if (username.isEmpty()){
        createDialogWarning("Please enter username");
    }
    else{
        m_username = username;
        ServerCommunicationManager::instance().createGame(username);
    }
}


void MainWindow::on_pbStartGame_lb_clicked()
{
    ServerCommunicationManager::instance().startGame();
}

void MainWindow::goToLobby(QString gameID, QVector<QString> players)
{
    setCurrentPage(Page::Lobby);
    ui->lblGameID->setText(gameID);

    QString playersText = players.join("\n");
    ui->tePlayers->setText(playersText);

}
void MainWindow::joinLobby(QString gameID, QVector<QString> players)
{
    setCurrentPage(Page::Lobby);
    if (m_username.compare(players[0]) != 0){
        ui->pbStartGame_lb->hide();

    }else{
        ui->pbStartGame_lb->show();
    }

    ui->lblGameID->setText(gameID);
    QString playersText = players.join("\n");
    ui->tePlayers->setText(playersText);

}

void MainWindow::startGame(QVector<QString> players)
{
    gameWindow = new GameWindow(players, players.indexOf(m_username), this, this->getImgPath());
    gameWindow->show();

    this->hide();


    audioOutput->setVolume(0);
}

void MainWindow::createDialogWarning(QString message)
{
    dialogWarning = new QDialog(this);
    dialogWarning->setWindowModality(Qt::WindowModality::ApplicationModal);
    dialogWarning->setMinimumWidth(250);
    dialogWarning->setMinimumHeight(100);

    QLabel *lbl = new QLabel();
    lbl->setGeometry(0, 50, 250, 40);
    lbl->setAlignment(Qt::AlignHCenter);

    lbl->setText(message);
    lbl->setParent(dialogWarning);
    lbl->show();

    dialogWarning->show();

    connect(dialogWarning, &QDialog::rejected, this, [=]() {
        lbl->deleteLater();
        dialogWarning->deleteLater();
        disconnect(dialogWarning, &QDialog::rejected, this, nullptr);
    });
}

void MainWindow::setImgPath(QString newImgPath)
{
    m_imgPath = newImgPath;
}


void MainWindow::on_pbBackground1_clicked()
{
    setImgPath("../../resources/images/gamewindow_background.jpg");
    ui->pbBackground1->setStyleSheet("border: 2px solid white;");
    ui->pbBackground2->setStyleSheet("border: none;");
    ui->pbBackground3->setStyleSheet("border: none;");
}

void MainWindow::on_pbBackground2_clicked()
{
    setImgPath("../../resources/images/gamewindow_background2.jpg");
    ui->pbBackground2->setStyleSheet("border: 2px solid white;");
    ui->pbBackground1->setStyleSheet("border: none;");
    ui->pbBackground3->setStyleSheet("border: none;");

}

void MainWindow::on_pbBackground3_clicked()
{
    setImgPath("../../resources/images/gamewindow_background3.jpg");
    ui->pbBackground3->setStyleSheet("border: 2px solid white;");
    ui->pbBackground1->setStyleSheet("border: none;");
    ui->pbBackground2->setStyleSheet("border: none;");
}

QString MainWindow::getImgPath() const
{
    return m_imgPath;
}

void MainWindow::on_volumeSlider_valueChanged(int value)
{
    if (audioOutput)
    {
        qreal volume = static_cast<qreal>(value) / 100.0;
        audioOutput->setVolume(volume);
    }
}

void MainWindow::on_pbBack_gr_clicked()
{
    setCurrentPage(Page::Preferences);
}


void MainWindow::on_pbGameRules_clicked()
{
    setCurrentPage(Page::GameRules);
}

void MainWindow::goToStartScreen()
{
    setCurrentPage(MainWindow::Page::Menu);
}

